﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets.ResourceLocators;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceLocations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;
using Ad = UnityEngine.AddressableAssets.Addressables;
using Object = UnityEngine.Object;

namespace Addressables
{
    public static class AssetManager {
        const string BASE_ERROR = "<color=#ffa500>" + nameof(AssetManager) + " Error:</color> ";

        public delegate void DelegateAssetLoaded(IResourceLocation location, AsyncOperationHandle handle);
        public static event DelegateAssetLoaded OnAssetLoaded;

        public delegate void DeletageAssetUnloaded(IResourceLocation location);
        public static event DeletageAssetUnloaded OnAssetUnloaded;

        public delegate void DelegateResourceLocationLoaded(string key, AsyncOperationHandle handle);
        public static event DelegateResourceLocationLoaded OnResourceLocationLoaded;

        static readonly Dictionary<IResourceLocation, AsyncOperationHandle> _loadingAssets = new Dictionary<IResourceLocation, AsyncOperationHandle>();

        static readonly Dictionary<IResourceLocation, AsyncOperationHandle> _loadedAssets = new Dictionary<IResourceLocation, AsyncOperationHandle>();

        static readonly Dictionary<string, AsyncOperationHandle> _loadingResourceLocations = new Dictionary<string, AsyncOperationHandle>();

        static readonly Dictionary<string, AsyncOperationHandle> _loadedResourceLocations = new Dictionary<string, AsyncOperationHandle>();

        public static IReadOnlyList<object> LoadedAssets => _loadedAssets.Values.Select(x => x.Result).ToList();

        static readonly Dictionary<IResourceLocation, List<GameObject>> _instantiatedObjects = new Dictionary<IResourceLocation, List<GameObject>>(10);

        public static int loadedAssetsCount => _loadedAssets.Count;

        public static int loadingAssetsCount => _loadingAssets.Count;

        public static int instantiatedAssetsCount => _instantiatedObjects.Values.SelectMany(x => x).Count();

        #region Get

        public static bool IsLoaded(IResourceLocation location) {
            return _loadedAssets.ContainsKey(location);
        }

        public static bool IsLoading(IResourceLocation location) {
            return _loadingAssets.ContainsKey(location);
        }

        public static bool IsInstantiated(IResourceLocation location) {
            return _instantiatedObjects.ContainsKey(location);
        }

        public static int InstantiatedCount(IResourceLocation location) {
            return IsInstantiated(location) ? _instantiatedObjects[location].Count : 0;
        }
        #endregion

        #region Load/Unload

        public static bool TryGetOrLoadObjectAsync<TObjectType>(IResourceLocation location, out AsyncOperationHandle<TObjectType> handle) where TObjectType : Object
        {
            if (_loadedAssets.ContainsKey(location)) {
                try
                {
                    handle = _loadedAssets[location].Convert<TObjectType>();
                }
                catch
                {

                    handle = Ad.ResourceManager.CreateCompletedOperation(_loadedAssets[location].Result as TObjectType, string.Empty);
                }

                return true;
            }

            if (_loadingAssets.ContainsKey(location)) {
                try
                {
                    handle = _loadingAssets[location].Convert<TObjectType>();
                }
                catch
                {
                    handle = Ad.ResourceManager.CreateChainOperation(_loadingAssets[location], chainOp => Ad.ResourceManager.CreateCompletedOperation(chainOp.Result as TObjectType, string.Empty));
                }

                return false;
            }

            handle = Ad.LoadAssetAsync<TObjectType>(location);

            _loadingAssets.Add(location, handle);

            handle.Completed += op =>
            {
                _loadedAssets.Add(location, op);
                _loadingAssets.Remove(location);

                OnAssetLoaded?.Invoke(location, op);
            };
            return false;
        }

        public static bool TryGetObjectSync<TObjectType>(IResourceLocation location, out TObjectType result) where TObjectType : Object
        {
            if (_loadedAssets.TryGetValue(location, out var handle)) {
                result = handle.Convert<TObjectType>().Result;
                return true;
            }

            result = null;
            return false;
        }

        public static bool TryGetOrLoadComponentAsync<TComponentType>(IResourceLocation location, out AsyncOperationHandle<TComponentType> handle) where TComponentType : Component
        {
            if (_loadedAssets.ContainsKey(location)) {
                handle = ConvertHandleToComponent<TComponentType>(_loadedAssets[location]);
                return true;
            }
            if (_loadingAssets.ContainsKey(location)) {
                handle = Ad.ResourceManager.CreateChainOperation(_loadingAssets[location], ConvertHandleToComponent<TComponentType>);
                return false;
            }

            var op = Ad.LoadAssetAsync<GameObject>(location);
            _loadingAssets.Add(location, op);
            op.Completed += op2 => {
                _loadedAssets.Add(location, op2);
                _loadingAssets.Remove(location);
                OnAssetLoaded?.Invoke(location, op2);
            };

            handle = Ad.ResourceManager.CreateChainOperation(op, chainOp => {
                return ConvertHandleToComponent<TComponentType>(chainOp);
            });
            return false;
        }

        public static bool TryGetComponentSync<TComponentType>(IResourceLocation location, out TComponentType result) where TComponentType : Component
        {
            if (_loadedAssets.ContainsKey(location)) {
                var handle = ConvertHandleToComponent<TComponentType>(_loadedAssets[location]);
                result = handle.Result;
                return true;
            }
            result = null;
            return false;
        }

        public static void UnloadAsset(IResourceLocation location) {
            AsyncOperationHandle handle;
            if (_loadingAssets.TryGetValue(location, out handle))
            {
                _loadingAssets.Remove(location);
            }
            else if (_loadedAssets.TryGetValue(location, out handle))
            {
                _loadedAssets.Remove(location);
            }
            else {
                throw new System.Exception($"");
            }

            if (IsInstantiated(location))
                DestroyAllInstances(location);

            Ad.Release(handle);
            OnAssetUnloaded?.Invoke(location);
        }

        public static bool TryGetResourceLocationAsync(string key, out AsyncOperationHandle<IList<IResourceLocation>> handle) {
            if (_loadedResourceLocations.ContainsKey(key)) {
                var list = _loadedResourceLocations[key].Result as IList<IResourceLocation>;
                handle = Ad.ResourceManager.CreateCompletedOperation(list, string.Empty);

                return true;
            }

            if (_loadingResourceLocations.ContainsKey(key)) {
                handle = Ad.ResourceManager.CreateChainOperation(_loadingResourceLocations[key], chainOp => {
                    var list = chainOp.Result as IList<IResourceLocation>;
                    return Ad.ResourceManager.CreateCompletedOperation(list, string.Empty);
                });
                return false;
            }

            handle = Ad.LoadResourceLocationsAsync(key);
            _loadingResourceLocations.Add(key, handle);

            handle.Completed += op => {
                _loadedResourceLocations.Add(key, op);
                _loadingResourceLocations.Remove(key);

                OnResourceLocationLoaded?.Invoke(key, op);
            };

            return false;
        }

        public static bool TryGetResourceLocationSync(string key, out IList<IResourceLocation> result) {
            if (_loadedResourceLocations.TryGetValue(key, out var handle)) {
                result = handle.Convert<IList<IResourceLocation>>().Result;
                return true;
            }

            result = null;
            return false;
        }

        #endregion

        #region Instantiation
        public static bool TryInstantiateOrLoadAsync(IResourceLocation location, Vector3 position, Quaternion rotation, Transform parent, out AsyncOperationHandle<GameObject> handle) {
            if (TryGetOrLoadObjectAsync(location, out AsyncOperationHandle<GameObject> loadHandle)) {
                var instance = InstantiateInternal(location, loadHandle.Result, position, rotation, parent);
                handle = Ad.ResourceManager.CreateCompletedOperation(instance, string.Empty);
                return true;
            }

            if (!loadHandle.IsValid()) {
                Debug.LogError($"Load Operation was invalid: {loadHandle}.");
                handle = Ad.ResourceManager.CreateCompletedOperation<GameObject>(null, $"Load Operation was invalid: {loadHandle}.");
                return false;
            }

            handle = Ad.ResourceManager.CreateChainOperation(loadHandle, chainOp =>
            {
                var instance = InstantiateInternal(location, chainOp.Result, position, rotation, parent);
                return Ad.ResourceManager.CreateCompletedOperation(instance, string.Empty);
            });
            return false;
        }

        public static bool TryInstantiateOrLoadAsync<TComponentType>(IResourceLocation location, Vector3 position, Quaternion rotation, Transform parent, out AsyncOperationHandle<TComponentType> handle) where TComponentType : Component
        {
            if (TryGetOrLoadComponentAsync(location, out AsyncOperationHandle<TComponentType> loadHandle)) {
                var instance = InstantiateInternal(location, loadHandle.Result, position, rotation, parent);
                handle = Ad.ResourceManager.CreateCompletedOperation(instance, string.Empty);
                return true;
            }
            if (!loadHandle.IsValid()) {
                Debug.LogError($"Load Operation was invalid: {loadHandle}.");
                handle = Ad.ResourceManager.CreateCompletedOperation<TComponentType>(null, $"Load Operation was invalid: {loadHandle}.");
                return false;
            }

            handle = Ad.ResourceManager.CreateChainOperation(loadHandle, chainOp => {
                var instance = InstantiateInternal(location, chainOp.Result, position, rotation, parent);
                return Ad.ResourceManager.CreateCompletedOperation(instance, string.Empty);
            });
            return false;
        }

        public static bool TryInstantiateMultiOrLoadAsync(IResourceLocation location, int count, Vector3 position, Quaternion rotation, Transform parent, out AsyncOperationHandle<List<GameObject>> handle) {
            if (TryGetOrLoadObjectAsync(location, out AsyncOperationHandle<GameObject> loadHandle)) {
                var list = new List<GameObject>(count);
                for (int i = 0; i < count; i++) {
                    var instance = InstantiateInternal(location, loadHandle.Result, position, rotation, parent);
                    list.Add(instance);
                }

                handle = Ad.ResourceManager.CreateCompletedOperation(list, string.Empty);
                return true;
            }

            if (!loadHandle.IsValid()) {
                Debug.LogError($"Load Operation was invalid: {loadHandle}.");
                handle = Ad.ResourceManager.CreateCompletedOperation<List<GameObject>>(null, $"Load Operation was invalid: {loadHandle}.");
                return false;
            }

            handle = Ad.ResourceManager.CreateChainOperation(loadHandle, chainOp => {
                var list = new List<GameObject>(count);
                for (int i = 0; i < count; i++)
                {
                    var instance = InstantiateInternal(location, loadHandle.Result, position, rotation, parent);
                    list.Add(instance);
                }

                return Ad.ResourceManager.CreateCompletedOperation(list, string.Empty);
            });
            return false;
        }

        public static bool TryInstantiateMultiOrLoadAsync<TComponentType>(IResourceLocation location, int count, Vector3 position, Quaternion rotation, Transform parent, out AsyncOperationHandle<List<TComponentType>> handle) where TComponentType : Component
        {
            if (TryGetOrLoadComponentAsync(location, out AsyncOperationHandle<TComponentType> loadHandle)) {
                var list = new List<TComponentType>(count);
                for (int i = 0; i < count; i++) {
                    var instance = InstantiateInternal(location, loadHandle.Result, position, rotation, parent);
                    list.Add(instance);
                }
                handle = Ad.ResourceManager.CreateCompletedOperation(list, string.Empty);
            }

            if (!loadHandle.IsValid()) {
                Debug.LogError($"Load Operation was invalid: {loadHandle}.");
                handle = Ad.ResourceManager.CreateCompletedOperation<List<TComponentType>>(null, $"Load Operation was invalid: {loadHandle}.");
                return false;
            }

            handle = Ad.ResourceManager.CreateChainOperation(loadHandle, chainOp => {
                var list = new List<TComponentType>(count);
                for (int i = 0; i < count; i++)
                {
                    var instance = InstantiateInternal(location, loadHandle.Result, position, rotation, parent);
                    list.Add(instance);
                }
                return Ad.ResourceManager.CreateCompletedOperation(list, string.Empty);
            });
            return false;
        }

        public static bool TryInstantiateSync(IResourceLocation location, Vector3 position, Quaternion rotation, Transform parent, out GameObject result) {
            if (!TryGetObjectSync(location, out GameObject loadResult)) {
                result = null;
                return false;
            }

            result = InstantiateInternal(location, loadResult, position, rotation, parent);
            return true;
        }

        public static bool TryInstantiateSync<TComponentType>(IResourceLocation location, Vector3 position, Quaternion rotation, Transform parent, out TComponentType result) where TComponentType : Component
        {
            if (!TryGetComponentSync(location, out TComponentType loadResult)) {
                result = null;
                return false;
            }

            result = InstantiateInternal(location, loadResult, position, rotation, parent);
            return true;
        }

        public static bool TryInstantiateMultiSync(IResourceLocation location, int count, Vector3 position, Quaternion rotation, Transform parent, out List<GameObject> result) {
            if (!TryGetObjectSync(location, out GameObject loadResult))
            {
                result = null;
                return false;
            }

            var list = new List<GameObject>(count);
            for (int i = 0; i < count; i++) {
                var instance = InstantiateInternal(location, loadResult, position, rotation, parent);
                list.Add(instance);
            }

            result = list;
            return true;
        }

        public static bool TryInstantiateMultiSync<TComponentType>(IResourceLocation location, int count, Vector3 position, Quaternion rotation, Transform parent, out List<TComponentType> result) where TComponentType : Component
        {
            if (!TryGetComponentSync(location, out TComponentType loadResult)) {
                result = null;
                return false;
            }

            var list = new List<TComponentType>(count);
            for (int i = 0; i < count; i++) {
                var instance = InstantiateInternal(location, loadResult, position, rotation, parent);
                list.Add(instance);
            }

            result = list;
            return true;
        }

        static GameObject InstantiateInternal(IResourceLocation location, GameObject loadedAsset, Vector3 position, Quaternion rotation, Transform parent) {
            var instance = Object.Instantiate(loadedAsset, position, rotation, parent);
            if (!instance) {
                throw new NullReferenceException($"Instantiated Object of type '{typeof(GameObject)}' is null.");
            }

            var monoTracker = instance.gameObject.AddComponent<MonoTracker>();
            monoTracker.location = location;
            monoTracker.OnDestroyed += TrackerDestroyed;

            if (!_instantiatedObjects.TryGetValue(location, out var list)) {
                list = new List<GameObject>(20);
                _instantiatedObjects.Add(location, list);
            }
            list.Add(instance);
            return instance;
        }

        static TComponentType InstantiateInternal<TComponentType>(IResourceLocation location, TComponentType loadedAsset, Vector3 position, Quaternion rotation, Transform parent) where TComponentType : Component {
            var instance = Object.Instantiate(loadedAsset, position, rotation, parent);
            if (!instance) throw new NullReferenceException($"Instantiated Object of type '{typeof(TComponentType)}' is null.");
            var monoTracker = instance.gameObject.AddComponent<MonoTracker>();
            monoTracker.location = location;
            monoTracker.OnDestroyed += TrackerDestroyed;

            if (!_instantiatedObjects.ContainsKey(location)) {
                _instantiatedObjects.Add(location, new List<GameObject>(20));
            }
            _instantiatedObjects[location].Add(instance.gameObject);
            return instance;
        }

        static void TrackerDestroyed(MonoTracker tracker) {
            if (_instantiatedObjects.TryGetValue(tracker.location, out var list)) {
                list.Remove(tracker.gameObject);
            }
        }


        public static void DestroyAllInstances(IResourceLocation location) {
            if (!_instantiatedObjects.TryGetValue(location, out var list)) return;

            for (int i = list.Count - 1; i >= 0; i--) {
                DestroyInternal(list[i]);
            }
            _instantiatedObjects[location].Clear();
            _instantiatedObjects.Remove(location);
        }

        static void DestroyInternal(GameObject go) {
            Ad.ReleaseInstance(go);
        }
        #endregion

        #region Utilities

        static AsyncOperationHandle<TComponentType> ConvertHandleToComponent<TComponentType>(AsyncOperationHandle handle) where TComponentType : Component
        {
            if (handle.Result is GameObject go) {
                TComponentType comp = go.GetComponent<TComponentType>();
                if (!comp) {
                    throw new ConversionException($"Cannot {nameof(go.GetComponent)} of Type {typeof(TComponentType)}");
                }
                var result = Ad.ResourceManager.CreateCompletedOperation(comp, string.Empty);
                return result;
            }
            else {
                throw new ConversionException($"Cannot convert {nameof(handle.Result)} to {nameof(GameObject)}");
            }
        }

        static List<object> GetKeysFromLocations(IEnumerable<IResourceLocation> locations) {
            List<object> keys = new List<object>(locations.Count());

            foreach (var locator in Ad.ResourceLocators) {
                foreach (var key in locator.Keys) {
                    bool isGuid = Guid.TryParse(key.ToString(), out var guid);
                    if (!isGuid) continue;

                    if (!TryGetLocationID(locator, key, out var id)) continue;

                    var matched = locations.Select(x => x.InternalId).ToList().Exists(x => x == id);
                    if (!matched) continue;

                    keys.Add(key);
                }
            }

            return keys;
        }

        static bool TryGetLocationID(IResourceLocator locator, object key, out string internalId) {
            internalId = string.Empty;
            var exists = locator.Locate(key, typeof(Object), out var locations);
            if (!exists) return false;
            if (locations.Count == 0) return false;
            if (locations.Count > 1) return false;

            internalId = locations[0].InternalId;
            return true;
        }
        #endregion
    }
}
